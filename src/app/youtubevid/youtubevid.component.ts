import { Component, OnInit, Sanitizer } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Youtube } from '../model/youtube';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { YoutubeService } from '../service/youtube.service';

@Component({
  selector: 'app-youtubevid',
  templateUrl: './youtubevid.component.html',
  styleUrls: ['./youtubevid.component.scss']
})
export class YoutubevidComponent implements OnInit {
  vidId: number;
  selectedVideo: Youtube;
  videoUrl: SafeUrl;
  videos: Youtube[];

  constructor(private activatedRoute: ActivatedRoute,
              private youtubeService: YoutubeService,
              private sanitizer: DomSanitizer) {
      this.vidId = this.activatedRoute.snapshot.params.vidId;
  }

  ngOnInit() {
    this.getAllURLs();
  }

  getAllURLs() {
    this.youtubeService.getAllURLs().pipe(take(1)).subscribe(data => {
      this.videos = data;
      if (this.vidId !== 0) {
        this.selectVideo(this.vidId);

      } else {
        this.selectVideo(this.videos[0].id);
      }
    });
  }

  selectVideo(id: number) {
    this.youtubeService.getById(id).pipe(take(1)).subscribe(data => {
      this.selectedVideo = data;
      this.sanitizeUrl(this.selectedVideo.youtubeUrl);
    });
  }

  sanitizeUrl(url: string) {
    this.videoUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
