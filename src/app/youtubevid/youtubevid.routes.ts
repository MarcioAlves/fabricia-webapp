import { Route } from '@angular/router';
import { YoutubevidComponent } from './youtubevid.component';

export const YoutubeVidRoutes: Route[] = [
    {
        path: '',
        component: YoutubevidComponent
    }
]