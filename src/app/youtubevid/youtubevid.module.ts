import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { YoutubevidComponent } from './youtubevid.component';
import { YoutubeVidRoutes } from './youtubevid.routes';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    YoutubevidComponent
  ],
  exports: [
    YoutubevidComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(YoutubeVidRoutes),
  ]
})
export class YoutubevidModule { }
