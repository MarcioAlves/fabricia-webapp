import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { YoutubevidComponent } from './youtubevid.component';

describe('YoutubevidComponent', () => {
  let component: YoutubevidComponent;
  let fixture: ComponentFixture<YoutubevidComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ YoutubevidComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoutubevidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
