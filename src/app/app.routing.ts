import { Routes, RouterModule } from '@angular/router';

import { MainComponent } from './main/main.component';

const routes: Routes = [
  { path: '', component: MainComponent, pathMatch: 'full' },
  { path: 'main', component: MainComponent }
];

export const routing = RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' });
