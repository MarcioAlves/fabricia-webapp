import { ProjectRoutes } from './project.routes';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { ProjectComponent } from './project.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [ ProjectComponent ],
  exports: [
    ProjectComponent,
    MatExpansionModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatPaginatorModule,
    ScrollingModule
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ProjectRoutes),
    MatExpansionModule,
    MatButtonModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatPaginatorModule,
    ScrollingModule
  ]
})
export class ProjectModule { }
