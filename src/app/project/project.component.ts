import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../service/project.service';
import { DomSanitizer } from '@angular/platform-browser';
import { take } from 'rxjs/operators';
import { ResumedProject } from '../model/resumed-project';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  resumedProjects: ResumedProject[];
  page: number;

  constructor(private activatedRoute: ActivatedRoute, private projectService: ProjectService, private sanitizer: DomSanitizer) {
    this.page = 0;
  }

  ngOnInit() {
    this.getProjects();
  }

  getProjects() {
    this.projectService.getAllProjects(this.page).pipe(take(1)).subscribe(data => {
      this.resumedProjects = data;
    });
  }
}
