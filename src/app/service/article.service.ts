import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, } from 'rxjs';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Article } from '../model/article';

const httpOptions = {
  header: new HttpHeaders({ 'content-type': 'application/json' })
};

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  private baseUrl = API_URL + '/article';
  private article: Article;
  private articles: Article[];
  private articletypeName: string;

  constructor(private http: HttpClient) {}

  getArtigo(id: number): Observable<Article> {
    const url = this.baseUrl + '/id/' + id;
    try {
      return this.http.get<Article>(url)
      .pipe(
        map((res: any) => this.article = res));

    } catch (error) {
      this.handleError(error);
    }
  }

  getLastArtigo(articleType: number): Observable<Article> {
    const url = this.baseUrl + '/last-by-type/' + articleType;
    try {
      return this.http.get<Article>(url)
        .pipe(
          map((res: any) => this.article = res));

    } catch (error) {
      this.handleError(error);
    }
  }

  getArtigosList(articleType: number): Observable<Article[]> {
    const url = this.baseUrl + '/list-by-type/' + articleType;
    try {
      return this.http.get<Article[]>(url)
        .pipe(
          map((res: any) => this.articles = res));

    } catch (error) {
      this.handleError(error);
    }
  }

  getArticleType(articleType: number): Observable<string> {
    const url = this.baseUrl + '/getArticleTypeNameById/' + articleType;
    return this.http.get(url, {responseType: 'text'})
      .pipe(
        map((res: string) => this.articletypeName = res));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }
}
