import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Picture } from '../model/picture';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

const httpOptions = {
  header: new HttpHeaders({ 'content-type': 'application/json' })
};

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class PictureService {
  private baseUrl = API_URL + '/picture';
  private pathUrl = '';
  private picture: Picture;

  constructor(private httpClient: HttpClient) { }

  getPictureUrl(pictureId: number): Observable<string> {
    const url = this.baseUrl + '/id/' + pictureId;
    try {
      return this.httpClient.get<string>(url)
      .pipe(
        map(
          (res: any) => this.pathUrl = res)
      );
    } catch (error) {
      this.handleError();
    }
  }

  getPicture(pictureId: number): Observable<Picture> {
    const url = this.baseUrl + '/full-by-id/' + pictureId;
    try {
      return this.httpClient.get<string>(url)
        .pipe(
          map((res: any) => this.picture = res)
        );
    } catch (error) {
      this.handleError();
    }
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }
}
