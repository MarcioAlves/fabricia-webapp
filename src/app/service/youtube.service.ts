import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Youtube } from '../model/youtube';

const httpOptions = {
  header: new HttpHeaders({ 'content-type': 'application/json' })
};

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {
  private baseUrl = API_URL + '/youtube';
  private youtubeVids: Youtube[];
  private youtube: Youtube;

  constructor(private httpClient: HttpClient) { }

  getAllURLs(): Observable<Youtube[]> {
    try {
      return this.httpClient.get<Youtube>(this.baseUrl).pipe(
        map((res: any) => this.youtubeVids = res)
      );
    } catch (error) {
      this.handleError(error);
    }
  }

  getById(id: number): Observable<Youtube> {
    const url = this.baseUrl + '/id/' + id;
    try {
      return this.httpClient.get<Youtube>(url).pipe(
        map((res: any) => this.youtube = res)
      );
    } catch (error) {
      this.handleError(error);
    }
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }
}
