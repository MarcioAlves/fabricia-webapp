import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Album } from '../model/album';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class AlbumService {
  private baseUrl = API_URL + '/album';
  private albums: Album[];
  private album: Album;

  constructor(private http: HttpClient) { }

  getAlbums(): Observable<Album[]> {
    const url = this.baseUrl;
    try {
      return this.http.get<Album[]>(url)
        .pipe(
          map(
            (res: any) => this.albums = res)
          );

    } catch (error) {
      this.handleError();
    }
  }

  getAlbum(id: number): Observable<Album> {
    const url = this.baseUrl + '/id/' + id;
    try {
      return this.http.get<Album>(url)
        .pipe(
          map(
            (res: any) => this.album = res)
          );

    } catch (error) {
      this.handleError();
    }
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }
}
