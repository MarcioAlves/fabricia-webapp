import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Testimonial } from '../model/testimonial';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';

const httpOptions = {
  header: new HttpHeaders({ 'content-type': 'application/json' })
};

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class TestimonialService {
  private baseUrl = API_URL + '/testimonial';
  private testimonials: Testimonial[];

  constructor(private httpClient: HttpClient) { }

  getAll(): Observable<Testimonial[]> {
    try {
      const url = this.baseUrl + '/all';
      return this.httpClient.get<Testimonial>(url).pipe(
        map((res: any) => this.testimonials = res)
      );
    } catch (error) {
      this.handleError(error);
    }
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }
}
