import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Resume } from '../model/resume';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

const httpOptions = {
  header: new HttpHeaders({ 'content-type': 'application/json' })
};

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ResumeService {

  private baseUrl = API_URL + '/resume';
  private resumes: Resume[];
  private resume: Resume;

  constructor(private httpClient: HttpClient) { }

  getById(id: number): Observable<Resume> {
    try {
      const url = this.baseUrl + '/id/' + id;
      return this.httpClient.get<Resume>(url).pipe(
        map((res: any) => this.resume = res)
      );

    } catch (error) {
      this.handleError(error);
    }
  }

  private handleError<T>(result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log('${operation} failed: ${error.message}');
      return of(result as T);
    };
  }
}
