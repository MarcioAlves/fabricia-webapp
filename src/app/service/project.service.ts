import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Project } from '../model/project';
import { ResumedProject } from '../model/resumed-project';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  private baseUrl = API_URL + '/project';
  private project: Project;
  private resumedProject: ResumedProject;
  private resumedProjects: ResumedProject[];
  private size: string;

  constructor(private http: HttpClient, private route: ActivatedRoute) {
    this.size = '5';
  }

  getProject(id: number): Observable<Project> {
    const url = this.baseUrl + '/id/' + id;
    try {
      return this.http.get<Project>(url)
      .pipe(
        map((res: any) => this.project = res));

    } catch (error) {
      this.handleError(error);
    }
  }

  getLastProject(): Observable<ResumedProject> {
    const url = this.baseUrl + '/last';
    try {
      return this.http.get<ResumedProject>(url)
      .pipe(
        map((res: any) => this.resumedProject = res));

    } catch (error) {
      this.handleError(error);
    }
  }

  getAllProjects(page: number): Observable<ResumedProject[]> {
    const url = this.baseUrl + '/list';
    // let params = new HttpParams().set('page', page.toString()).set('size', this.size);
    try {
      return this.http.get<ResumedProject[]>(url)
      .pipe(
        map((res: any) => this.resumedProjects = res));
        
    } catch (error) {
      this.handleError(error);
    }
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    }
  }
}
