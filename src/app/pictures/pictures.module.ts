import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PictureComponent } from './picture.component';
import { RouterModule } from '@angular/router';
import { PicturesRoutes } from './pictures.routes';

@NgModule({
  declarations: [
    PictureComponent
  ],
  exports: [
    PictureComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(PicturesRoutes),
  ]
})
export class PicturesModule { }
