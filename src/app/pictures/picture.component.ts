import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Album } from '../model/album';
import { Picture } from '../model/picture';
import { PictureArray } from './picture-array';
import { PictureService } from '../service/picture.service';
import { AlbumService } from '../service/album.service';

@Component({
  selector: 'app-fotos',
  templateUrl: './picture.component.html',
  styleUrls: ['./picture.component.scss']
})
export class PictureComponent implements OnInit {
  albums: Album[];
  selectedAlbum: Album;
  selectedPicture: Picture;
  pictureArrays: PictureArray[];

  constructor(private activatedRoute: ActivatedRoute,
              private albumService: AlbumService,
              private pictureService: PictureService) { }

  ngOnInit() {
    this.getAlbums();
  }

  getAlbums() {
    this.albumService.getAlbums().pipe(take(1)).subscribe(data => {
      this.albums = data;
      this.getAlbum(this.albums[0].id);
      this.getPicture(this.albums[0].pictures[0].id);
    });
  }

  getAlbum(id: number) {
    this.albumService.getAlbum(id).pipe(take(1)).subscribe(data => {
      this.selectedAlbum = data;
      this.pictureArrays = this.selectedAlbum.pictureArrays;
      this.selectedPicture = this.selectedAlbum.pictures[0];
    });
  }

  getPicture(id: number) {
    this.pictureService.getPicture(id).pipe(take(1)).subscribe(data => {
      this.selectedPicture = data;
    });
  }
}
