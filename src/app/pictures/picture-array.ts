import { Picture } from '../model/picture';

export interface PictureArray {
  picture: Picture[];
  active: boolean;
}
