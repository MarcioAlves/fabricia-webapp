import { Route } from '@angular/router';
import { PictureComponent } from './picture.component';

export const PicturesRoutes: Route[] = [
    {
        path: '',
        component: PictureComponent
    }
]