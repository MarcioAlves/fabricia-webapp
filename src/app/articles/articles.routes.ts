import { Route } from '@angular/router';
import { ArticlesComponent } from './articles.component';

export const ArticlesRoutes: Route[] = [
    {
        path: '',
        component: ArticlesComponent
    }
]