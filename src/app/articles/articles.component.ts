import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { Picture } from '../model/picture';
import { take } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';
import { PictureService } from '../service/picture.service';
import { Article } from '../model/article';
import { ArticleService } from '../service/article.service';

@Component({
  selector: 'app-artigos',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.scss']
})
export class ArticlesComponent implements OnInit {
  article: Article;
  articles: Article[];
  articleType: number;
  articleName: string;
  background: SafeStyle;
  picture: Picture;

  constructor(private activeRoute: ActivatedRoute,
              private articleService: ArticleService,
              private pictureService: PictureService,
              private sanitizer: DomSanitizer) {
    this.articleType = this.activeRoute.snapshot.params.articleType;
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.getArticleType();
    this.getArtigos();
    this.getLastArtigo();
  }

  getArticle(id: number) {
    this.articleService.getArtigo(id).pipe(take(1)).subscribe(data => {
      this.article = data;
      this.getBackground(this.article.id);
    });
  }

  getLastArtigo() {
    this.articleService.getLastArtigo(this.articleType).pipe(take(1)).subscribe(data => {
      this.article = data;
      this.getBackground(this.article.backgroundPathId);
    });
  }

  getArtigos() {
    this.articleService.getArtigosList(this.articleType).pipe(take(1)).subscribe(data => {
      this.articles = data;
    });
  }

  getArticleType() {
    this.articleService.getArticleType(this.articleType).pipe(take(1)).subscribe(data => {
      this.articleName = data;
    });
  }

  getBackground(id: number) {
    this.pictureService.getPicture(id).pipe(take(1)).subscribe(data => {
      this.picture = data;
      this.background = this.sanitizer.bypassSecurityTrustStyle('url(' + this.picture.pathUrl + ')');
    });
  }
}
