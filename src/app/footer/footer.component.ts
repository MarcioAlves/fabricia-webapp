import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { take } from 'rxjs/operators';
import { Testimonial } from '../model/testimonial';
import { Resume } from '../model/resume';
import { TestimonialService } from '../service/testimonial.service';
import { ResumeService } from '../service/resume.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  shouldShowTestimonialModal: boolean;
  shouldShowResumeModal: boolean;

  testimonials: Testimonial[];
  resume: Resume;

  constructor(private testimonialService: TestimonialService,
              private resumeService: ResumeService,
              private sanitizer: DomSanitizer) {
      this.shouldShowTestimonialModal = false;
      this.shouldShowResumeModal = false;
    }

  ngOnInit() { }

  showTestimonialModal() {
    if (this.testimonials.length <= 0) {
      this.getTestimonials();
    }

    if (this.shouldShowTestimonialModal) {
      this.shouldShowTestimonialModal = false;

    } else {
      this.shouldShowTestimonialModal = true;
    }
  }

  showResumeModal() {
    if (this.resume !== null || this.resume.text !== 'undefined') {
      this.getResume();
    }

    if (this.shouldShowResumeModal) {
      this.shouldShowResumeModal = false;

    } else {
      this.shouldShowResumeModal = true;
    }
  }

  private getResume() {
    this.resumeService.getById(1).pipe(take(1)).subscribe(data => {
      this.resume = data;
    });
  }

  private getTestimonials() {
    this.testimonialService.getAll().pipe(take(1)).subscribe(data => {
      this.testimonials = data;
    });
  }
}
