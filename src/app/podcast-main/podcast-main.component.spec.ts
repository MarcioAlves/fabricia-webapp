import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PodcastMainComponent } from './podcast-main.component';

describe('PodcastMainComponent', () => {
  let component: PodcastMainComponent;
  let fixture: ComponentFixture<PodcastMainComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PodcastMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PodcastMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
