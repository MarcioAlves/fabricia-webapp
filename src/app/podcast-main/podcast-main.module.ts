import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PodcastMainComponent } from './podcast-main.component';
import { RouterModule } from '@angular/router';
import { PodcastMainRoutes } from './podcast-main.routes';

@NgModule({
  declarations: [PodcastMainComponent],
  exports: [
    PodcastMainComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(PodcastMainRoutes),
  ]
})
export class PodcastMainModule { }
