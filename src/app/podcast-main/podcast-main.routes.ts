import { Route } from '@angular/router';
import { PodcastMainComponent } from './podcast-main.component';

export const PodcastMainRoutes: Route[] = [
    {
        path: '',
        component: PodcastMainComponent
    }
]