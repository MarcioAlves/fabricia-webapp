import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    pathMatch: 'full'
  },
  {
    path: 'main',
    component: MainComponent
  },
  {
    path: 'articles/:articleType',
    loadChildren: () => import('./articles/articles.module').then(m => m.ArticlesModule),
  },
  {
    path: 'contos/:articleType',
    loadChildren: () => import('./articles/articles.module').then(m => m.ArticlesModule)
  },
  {
    path: 'memories/:articleType',
    loadChildren: () => import('./articles/articles.module').then(m => m.ArticlesModule)
  },
  {
    path: 'projects',
    loadChildren: () => import('./project/project.module').then(m => m.ProjectModule)
  },
  {
    path: 'projects/project',
    loadChildren: () => import('./project-detail/project.module').then(m => m.ProjectModule)
  },
  {
    path: 'podcastmain',
    loadChildren: () => import('./podcast-main/podcast-main.module').then(m => m.PodcastMainModule)
  },
  {
    path: 'podcastmain/podcast',
    loadChildren: () => import('./podcast/podcast.module').then(m => m.PodcastModule)
  },
  {
    path: 'fotos',
    loadChildren: () => import('./pictures/pictures.module').then(m => m.PicturesModule)
  },
  {
    path: 'youtubevid/:vidId',
    loadChildren: () => import('./youtubevid/youtubevid.module').then(m => m.YoutubevidModule)
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload', relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
