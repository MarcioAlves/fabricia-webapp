export interface Resume {
    id: number;
    title: string;
    text: string;
}
