import { Album } from './album';
import { Article } from './article';

export interface Project {
    id: number;
    title: string;
    albumTO: Album;
    articleTO: Article;
    cardPicture: string;
}
