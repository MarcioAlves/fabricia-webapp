export interface Youtube {
  id: number;
  youtubeUrl: string;
  title: string;
  description: string;
  pictureUrl: string;
}
