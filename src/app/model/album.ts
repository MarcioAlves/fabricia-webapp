import { Picture } from './picture';
import { PictureArray } from '../pictures/picture-array';

export interface Album {
  id: number;
  title: string;
  description: string;
  pictures: Picture[];
  pictureArrays: PictureArray[];
  eventDate: Date;
}
