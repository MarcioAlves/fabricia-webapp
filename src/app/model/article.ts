export interface Article {
    id: number;
    title: string;
    text: string;
    thumbPath: string;
    backgroundPath: string;
    backgroundPathId: number;
}
