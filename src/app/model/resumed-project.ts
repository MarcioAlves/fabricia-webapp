export interface ResumedProject {
    id: number;
    title: string;
    resume: string;
    cardPicture: string;
}
