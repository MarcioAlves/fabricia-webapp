export interface Picture {
  id: number;
  title: string;
  pathUrl: string;
  credits: string;
}
