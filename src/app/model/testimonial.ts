export interface Testimonial {
    id: number;
    name: string;
    role: string;
    text: string;
    pictureUrl: string;
}
