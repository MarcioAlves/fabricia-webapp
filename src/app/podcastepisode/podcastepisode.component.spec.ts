import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { PodcastepisodeComponent } from './podcastepisode.component';

describe('PodcastepisodeComponent', () => {
  let component: PodcastepisodeComponent;
  let fixture: ComponentFixture<PodcastepisodeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ PodcastepisodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PodcastepisodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
