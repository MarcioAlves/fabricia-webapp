import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { take } from 'rxjs/operators';
import { ResumedProject } from '../model/resumed-project';
import { Youtube } from '../model/youtube';
import { ProjectService } from '../service/project.service';
import { YoutubeService } from '../service/youtube.service';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  background: SafeStyle;
  //podcasts: Podcast[];//TODO - refactor podcast feature
  resumedProject: ResumedProject;
  youtubevids: Youtube[];

  constructor(private youtubeService: YoutubeService,
              private projectService: ProjectService,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.getBackground();
    this.projectService.getLastProject().pipe(take(1)).subscribe(data => {
      this.resumedProject = data;
    });

    this.youtubeService.getAllURLs().pipe(take(1)).subscribe(data => {
      this.youtubevids = data;
    });
  }

  getBackground() {
    let backgroundString: string;
    backgroundString = './assets/images/fundo.jpg';
    this.background = this.sanitizer.bypassSecurityTrustStyle('url(' + backgroundString + ')');
  }
}
