import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { take } from 'rxjs/operators';
import { Project } from '../model/project';
import { ProjectService } from '../service/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {
  project: Project;
  id: number;

  constructor(private activeRoute :ActivatedRoute,
              private projectService: ProjectService,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.id = params.id;
    })
    this.getProject(this.id);
  }

  getProject(id: number) {
    this.projectService.getProject(id).pipe(take(1)).subscribe(data => {
      this.project = data;
    });
  }
}
