/*
 * This class represents a podcast episode
 */
export class Podcast {
  id: number;
  title: string;
  description: string;
  thumbPath: string;
  backgroundPath: string;
  episodePath: string;
  episodePublicationDate: string;
  episodeLength: string;
  type: string;

  /*
   * Use this method to set the id of the podcast
   */
  setId(id: number) {
    this.id = id;
  }

  /*
   * Use this method to set the title of the podcast
   */
  setTitle(title: string) {
    this.title = title;
  }

  /*
   * Use this method to set the description of the podcast
   */
  setDescription(description: string) {
    this.description = description;
  }

  /*
   * Use this method to set the thumbnail path of the podcast
   */
  setThumbPath(thumbPath: string) {
    this.thumbPath = thumbPath;
  }

  /*
   * Use this method to set background path of the podcast
   */
  setBackgroundPath(backgroundPath: string) {
    this.backgroundPath = backgroundPath;
  }

  setEpisodePath(episodePath: string) {
    this.episodePath = episodePath;
  }

  setEpisodePublicationDate(episodePublicationDate: string) {
    this.episodePublicationDate = episodePublicationDate;
  }

  setEpisodeLength(episodeLength: string) {
    this.episodeLength = episodeLength;
  }

  /*
   *Use this method to set the type of the podcast
   */
  setType(type: string) {
    this.type = type;
  }
}
