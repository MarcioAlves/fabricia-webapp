import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Podcast } from '../podcast/podcast';

@Injectable({
  providedIn: 'root'
})
export class PodcastService {
  private lastPodcast = new Podcast();
  private lastNonSleepPodcast = new Podcast();

  constructor(private http: HttpClient) { }

  getPodcast(id: number): Podcast {
    return this.getPodcastArray()[id];
  }

  getPodcasts(type: string): Podcast[] {
    return this.getPodcastsByType(type);
  }

  private getPodcastsByType(type: string): Podcast[] {
    if (type === 'infantil') {
      return this.getCommonPodcasts();

    } else {
      return this.getNonSleepPodcasts();
    }
  }

  private getCommonPodcasts(): Podcast[] {
    const podcasts: Podcast[] = this.getPodcastArray();
    const toReturn: Podcast[] = new Array();

    podcasts.forEach((podcast) => {
      if (podcast.type === 'infantil') {
        toReturn.push(podcast);
      }
    });

    this.lastPodcast = toReturn[0];
    return toReturn;
  }

  private getNonSleepPodcasts(): Podcast[] {
    const podcasts: Podcast[] = this.getPodcastArray();
    const toReturn: Podcast[] = new Array();

    podcasts.forEach((podcast) => {
      if (podcast.type === 'nonSleep') {
        toReturn.push(podcast);
      }
    });

    this.lastNonSleepPodcast = toReturn[0];
    return toReturn;
  }

  private getPodcastArray(): Podcast[] {
    const podcasts: Podcast[] = new Array();
    let podcast = new Podcast();

    const max = 5;
    let counter = 0;
    let innerCounter = 10;

    for (counter; counter < max; counter++) {
      podcast = new Podcast();
      podcast.setId(counter);
      podcast.setTitle('Episódio do podcast ' + counter);
      podcast.setDescription('Aqui vai o texto do podcast ' + counter +
       '. I should be incapable of drawing a single stroke at the present moment;' +
       ' and yet I feel that I never was a greater artist than now. When, while the ' +
       'lovely valley teems with vapour around me, and the meridian sun strikes the upper' +
       ' surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary.');

      if (counter % 2) {
        podcast.setEpisodePath('../assets/audio/one.mp3');
        podcast.setEpisodeLength('66 minutos');
        podcast.setEpisodePublicationDate('31 de maio de 2019');

      } else {
        podcast.setEpisodePath('../assets/audio/two.mp3');
        podcast.setEpisodeLength('103 minutos');
        podcast.setEpisodePublicationDate('19 de abril de 2019');
      }

      if (innerCounter < 6) {
        if (innerCounter === 5) {
          podcast.setThumbPath('https://static.pingendo.com/img-placeholder-' + 1 + '.svg');

        } else if (innerCounter > 4) {
          podcast.setThumbPath('https://static.pingendo.com/img-placeholder-' + (innerCounter - 1) + '.svg');

        } else {
          podcast.setThumbPath('https://static.pingendo.com/img-placeholder-' + (innerCounter) + '.svg');
        }

        podcast.setType('nonSleep');

      } else {
        if (counter === 0) {
          podcast.setThumbPath('https://static.pingendo.com/img-placeholder-' + 1 + '.svg');

        } else if (counter > 4) {
          podcast.setThumbPath('https://static.pingendo.com/img-placeholder-' + (counter - 1) + '.svg');

        } else {
          podcast.setThumbPath('https://static.pingendo.com/img-placeholder-' + (counter) + '.svg');
        }

        podcast.setType('infantil');
      }

      podcast.setBackgroundPath(podcast.thumbPath);
      podcasts.push(podcast);
    }

    this.lastPodcast = podcasts[0];

    innerCounter--;
    return podcasts;
  }
}
