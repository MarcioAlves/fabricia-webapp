import { Route } from '@angular/router';
import { PodcastComponent } from './podcast.component';

export const PodcastRoutes: Route[] = [
    {
        path: '',
        component: PodcastComponent
    }
];
