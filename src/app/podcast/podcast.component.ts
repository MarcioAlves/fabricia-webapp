import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Podcast } from './podcast';
import { PodcastService } from './podcast.service';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';
import { initPlayer } from '../../assets/js/audio-player.js';
import { stopPlayer } from '../../assets/js/audio-player.js';

@Component({
  selector: 'app-podcast',
  templateUrl: './podcast.component.html',
  styleUrls: ['./podcast.component.scss']
})
export class PodcastComponent implements OnInit {

  podcastType: string;
  background: SafeStyle;
  podcasts: Podcast[];
  selectedPodcast: Podcast;

  constructor(private activatedRoute: ActivatedRoute,
              private podcastService: PodcastService,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.podcastType = params.podcastType;
    });
    this.getData();
    initPlayer();
  }

  getPodcast(id: number) {
    stopPlayer();
    this.selectedPodcast = this.podcastService.getPodcast(id);
    initPlayer();
  }

  getData() {
    let backgroundImage: string;

    if (this.podcastType === 'infantil') {
      backgroundImage = 'img-placeholder-1.svg';

    } else if (this.podcastType === '' || this.podcastType === undefined) {
      this.podcastType = 'infantil';

    } else {
      backgroundImage = 'img-placeholder-2.svg';
    }

    this.background = this.sanitizer.bypassSecurityTrustStyle(
        'url(https://static.pingendo.com/' + backgroundImage + ')');
    this.podcasts = this.podcastService.getPodcasts(this.podcastType);
    this.selectedPodcast = this.podcasts[0];
  }

}
