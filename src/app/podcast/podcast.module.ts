import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PodcastComponent } from './podcast.component';
import { PodcastRoutes } from './podcast.routes';

@NgModule({
  declarations: [PodcastComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(PodcastRoutes),
  ]
})
export class PodcastModule { }
